#include<bits/stdc++.h>

using namespace std;

class Genetics{
	public:
	string getOffspring(string g1, string g2, string dom){
		string offspring;
		for(int i=0;i<g1.length();i++){
			if(g1[i] == g2[i]){
				offspring.append(g1[i]);
			}
			else{
				if(dom[i] == 'R'){
					offspring.append(((int)g1[i]>(int)g2[i])?g1[i]:g2[i]);
				}
				else{
					offspring.append(((int)g1[i]>(int)g2[i])?g2[i]:g1[i]);
				}
			}
		}
		return offspring;
	}
};
