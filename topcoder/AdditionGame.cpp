#include<iostream>

using namespace std;

public class AdditionGame{
	public:
	int getMaximumPoints(int A,int B,int C,int N){
		int max_pnt;
		while(N){
			max_pnt += (A>B)?((A>C)?(A?A--:A):(C?C--:C)):((B>C)?(B?B--:B):(C?C--:C));
			N--;	
		}
		return max_pnt;
	}
}t;

int main(){
	int a,b,c,n;
	cout<<"Enter 3 numbers: ";
	cin>>a>>b>>c;
	cout<<"Enter the number of times the game is played:";
	cin>>n;
	int max = t.getMaximumPoints(a,b,c,n);
	cout<<"Maximum Possible point is:"<<max<<"\n";
	return 0;	
}
