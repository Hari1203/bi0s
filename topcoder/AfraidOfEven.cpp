#include<bits/stdc++.h>

using namespace std;

class AfraidOfEven{
	public:
	vector <int> restoreProgression(vector <int> seq){
		if(seq[1]-seq[0] == seq[2]-seq[1])
			return seq;
		vector<int> s;
		s = seq;
		int d,d1=abs(seq[2]-seq[0])/2,d2=abs(seq[3]-seq[1])/2,k=0,check=0;
		bool is_true=false;
		for(int i=0 ; i<seq.size() ; i++){
			d = (i%2)?d1:d2;
			if(i == seq.size()-1)
				k=1;
			while(!(check)){
				s[i] *= 2;
				if(d == abs(s[i+1-k]-s[i-k])){
					is_true = true;
					check = 1;
					seq = s;
				}
			}
			if(is_true)
				i++;
			s = seq;
			check = 0;
		}
		return seq;
	}
}t;

int main(){
	int a[] = {7, 47, 5, 113, 73, 179, 53};
	vector<int> seq(a,a+sizeof(a)/sizeof(a[0]));
	seq=t.restoreProgression(seq);
	for(int i=0 ; i<seq.size() ; i++){
		cout<<seq[i]<<' '; 
	}
	cout<<endl;
	return 0;
}
