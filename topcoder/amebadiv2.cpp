#include<bits/stdc++.h>

using namespace std;

class AmebaDiv2{
	public:
	int simulate(vector<int> X,int A){
		for(int i=0 ; i < X.size() ; i++){
			A += ((A==X[i])?X[i]:0);
		}
		return A;		
	}
};

int main(){
	int A=63;
	AmebaDiv2 t;
	int a[] = {817,832,817,832,126,817,63,63,126,817,832,287,823,817,574};
	vector<int> X (a,a+sizeof(a)/sizeof(a[0]));
	A = t.simulate(X,A);
	cout<<A<<endl;
	return 0;
}
