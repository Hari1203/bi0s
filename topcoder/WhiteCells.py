class WhiteCells:
	def countOccupied(self,board):
		count = 0
		white_pos = 1
		for i in range(8):
			if(white_pos):
				k = 0
			else:
				k = 1
			for j in range(k,8,2):
				if(board[i][j] == 'F'):
					count += 1
				white_pos = not(white_pos)
		return count
ob = WhiteCells()
brd = {"FFFFFFFF", "FFFFFFFF", "FFFFFFFF", "FFFFFFFF", "FFFFFFFF", "FFFFFFFF", "FFFFFFFF", "FFFFFFFF"}
print(ob.countOccupied)
