#include<bits/stdc++.h>

using namespace std;

class AimToTen{
	public:
		int need(vector<int>marks){
			int count = 0;
			float avg = accumulate(marks.begin(),marks.end(),0)*1.0/marks.size();
			while( avg < 9.5 ){
				marks.push_back(10);
				count++;
				avg = accumulate(marks.begin(),marks.end(),0)*1.0/marks.size();
			}
			return count;
		}
}t;

int main(){
	int a[] = {8, 9, 0};
	vector<int> marks(a,a+sizeof(a)/sizeof(a[0]));
	int count = t.need(marks);
	cout<<count<<endl;
	return 0;	
}
