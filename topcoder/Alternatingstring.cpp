#include<bits/stdc++.h>

using namespace std;

class  AlternatingString{
	public:
	int maxLength(string s){
		int count=1,max_count=1;
		for(int i=0;i<s.size()-1;i++){
			if(s[i] != s[i+1]){
				count++;
				if(count > max_count)
					max_count=count;
			}
			else 
				count = 1;
		}
		return max_count;
	}
}t;

int main(){
	string s = "1011011110101010010101";
	int count=t.maxLength(s);
	cout<<count<<endl;
	return 0;
}

