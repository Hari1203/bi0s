class AimToTen:
	def need(self,marks):
		count = 0
		avg = sum(marks)/len(marks)
		while(avg < 9.5):
			marks = marks + (10,)
			count += 1
			avg = sum(marks)/len(marks)
		return count
