#include<bits/stdc++.h>

using namespace std;

class BacteriesColony{
	public:
	vector <int> performTheExperiment(vector <int> colonies){
		bool is_over = false,is_changed;
		vector<int> v;
		while(!(is_over)){
			is_changed = false;
			v = colonies;
			for(int i=1;i < colonies.size()-1;i++){
				if(colonies[i] > v[i-1] and colonies[i] > v[i+1]){
					colonies[i]--;
					is_changed = true;
				}
				else
					if(colonies[i] < v[i-1] and colonies[i] < v[i+1]){
                                        	colonies[i]++;
						is_changed = true;
                                	}
			}
			if(!(is_changed))
				is_over = true;
		}
		return colonies;
	}
};

int main(){
	BacteriesColony t;
	int a[]={32, 68, 50, 89, 34, 56, 47, 30, 82, 7, 21, 16, 82, 24, 91 };
	vector<int> v(a,a+sizeof(a)/sizeof(a[0]));
	v = t.performTheExperiment(v);
	for(int i = 0;i < v.size() ; i++){
		cout<<v[i]<<' ';
	}
	cout<<endl;
	return 0;
}
