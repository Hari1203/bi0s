#include<bits/stdc++.h>

using namespace std;

class ApplePie{
	public:
	int getApples(vector<int> x, vector<int> y, vector<int> day, int howMany){
		int count = 0;
		for(int i=0; i < x.size(); i++){
			if(x[i] >= 0 && y[i] >=0 && x[i] <= 100 && y[i] <= 100){
				count++;	
			}
			if(count == howMany)
				return day[i];
		}
		return -1;
	}
}t;

int main(){
	int a[] = {0,50,600,50}; 
	int b[] = {0,-400,100,40};
	int c[] = {1,4,6,8};
	int howMany = 2,value;
	vector<int> x(a,a+sizeof(a)/sizeof(a[0]));
	vector<int> y(b,b+sizeof(b)/sizeof(b[0]));
	vector<int> day(c,c+sizeof(c)/sizeof(c[0]));
	value = t.getApples(x,y,day,howMany);
	cout<<value<<endl;
}
