#include<bits/stdc++.h>

#define PI 3.14159265

using namespace std;

class Airways{
	public :
	long double angle;
	double dist;
	double distance(int n,int east,int north){
		long double angle1=0,angle2=360.0/n,x=east,y=north;
		if( y == 0 and x < 0 )
			angle = 180;
		else
			angle = atan((float)y/x)*180/PI;
		while(angle1 <= 360){
			if(angle == angle1 or angle == angle2){
				dist = dist_2_pnts(x,y,0,0);
				return dist;
			}
			if(angle1 < angle and angle < angle2)
				break;
			angle1 = angle2;
			angle2 += 360/n;
		}
		long double m1,a1,b1,c1;
		if(angle1 == 90 or angle1 == 270){
			a1 = 1;
			b1 = 0;
			c1 = x;
		}
		else{
			m1 = tan(angle1*PI/180);
			a1 = -m1;
			b1 = 1;
			c1 = y-m1*x;
		}
		long double m2,a2,b2,c2;
		if(angle2 == 90 or angle2 == 270){
			a2 = 1;
			b2 = 0;
			c2 = 0;
		}
		else{
			m2 = tan(angle2*PI/180);
			a2 = -m2;
			b2 = 1;
			c2 = 0;
		}
		long double x1,y1;
		x1 = ((c1*b2) - (b1*c2))/((a1*b2) - (b1*a2));
		y1 = ((a1*c2) - (c1*a2))/((a1*b2) - (b1*a2));
		dist = dist_2_pnts(x1,y1,0,0)+dist_2_pnts(x,y,x1,y1);
		return dist;
	}
	
	double dist_2_pnts(long double x1,long double y1,long double x2,long double y2){
		return sqrt(pow(x2-x1,2)+pow(y2-y1,2));
	}
}t;

int main(){
	int n,east,north;
	cout<<"Enter the number of possible directions: ";
	cin>>n;
	cout<<"Enter directions coordinates for east and north:";
	cin>>east>>north;
	double min_dist = t.distance(n,east,north);
	cout<<"The minimum distance:"<<fixed<<setprecision(15)<<min_dist<<endl;
	return 0;
}
