#include<bits/stdc++.h>

using namespace std;

class AnswerEvaluation{
	public:
	int getScore(vector <string> keywords, vector <int> scores, string answer){
		int tot_score=0;
		stringstream s(answer);
		istream_iterator<string> begin(s);
		istream_iterator<string> end;
		vector<string> words(begin, end);
		for(int i = 0 ; i<words.size() ; i++){	
			for(int j = 0 ; j<keywords.size() ; j++){
				if(words[i] == keywords[j]){
					tot_score += scores[j];
					scores[j] = 0;
				}
			}
		}
		return tot_score;
	}
}t;

int main(){
	const char* words[] = {"red", "fox", "lazy", "dogs"};
	vector<string> key(words, words + 4);
	int a[] = {25,25,25,25};
	vector<int> scores(a,a + sizeof(a)/sizeof(a[0]));	
	string ans = "the quick brown fox jumped over the lazy dog";
	int total = t.getScore(key,scores,ans);
	cout<<total<<endl;
	return 0; 
}
