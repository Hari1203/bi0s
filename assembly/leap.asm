extern printf
extern scanf

section .data
	prompt : db "Enter current year:",10,0
	output : db "The next 20 leap years are:",10,0 
	out: db "%s",0
	num: db "%d",0
	space: db " ",0
	i:dd 0
	end : db "",10,0
	year: dd 0
	

section .text 
	global main
	main:
		push ebp
		mov ebp,esp

		push prompt
		call printf
		
		push year 
		push num
		call scanf
		
		push output
		push out
		call printf

		mov DWORD[i],0

		l1:
			mov eax,DWORD[year]
			cdq
			mov ebx,400
			idiv ebx
			cmp edx,0
			je l2
			mov eax,DWORD[year]
                        cdq
                        mov ebx,4
			idiv ebx
			cmp edx,0
			je l3
			jmp l4
			
		l2:
			inc DWORD[i]

			push DWORD[year]
			push num
			call printf

			push space
			call printf

			cmp DWORD[i],20
			je l5
			
			inc DWORD[year]
			jmp l1
		l3: 
			mov eax,DWORD[year]
                        cdq
                        mov ebx,100
			idiv ebx
			cmp edx,0
			je l4

			push DWORD[year]
			push num
			call printf

			push space 
			call printf

			inc DWORD[i]

			cmp DWORD[i],20
			je l5
			
			inc DWORD[year]
			jmp l1
		l4: 
			inc DWORD[year]
			jmp l1
		l5:	
			push end
			call printf
			leave
			ret	
