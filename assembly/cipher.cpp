#include<stdio.h>
#include<stdlib.h>
#include<string.h>
int main(int argc,char *argv[]){
	char *str=argv[1];
	int env;
	if(getenv("rot") == NULL){
		env = 13;
	}
	else{
		env = atoi(getenv("rot"));
	}
	int t;
	for(int i=0 ; str[i] != '\0' ; i++){
		if(str[i] >= 65 && str[i] <= 90){
		 	t=str[i];
		 	t += env;
			if(t > 90){
				t -= 26;	
			}
			str[i] = t;
		}
		else{
			if(str[i] >= 97 && str[i] <= 122){
				t=str[i];
		 		t += env;
				if(t > 122){
					t -= 26;	
				}
				str[i] = t;
			}
		}
	}
	printf("Encrypted text: ");
	printf("%s",str);
	printf("\n");
	return 0;
}

