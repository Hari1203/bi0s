extern scanf 
extern printf

section .data
	promt: db "Enter a number:" ,10,0
	output: db "Sum is :",10,0
	strng : db "%s" ,0
	num: db "%d",0
	n: db  0

section .text
	global main
	main:
		push ebp
		mov ebp,esp

		push promt
		call printf

		push n
		push num
		call scanf

		mov ecx,0x1
		mov ebx,0x0
		l2:
		add ebx,ecx
		inc ecx
		cmp ecx,DWORD [n]
		jle l2

		push output
		call printf

		push ebx
		push num
		call printf

		leave 
		ret
		
