# Functions

## What is Function?
Think of a situation where you do a particualar job over and over again. Now what if you could teach someone else to do the job and whenever you need that particular task done you can call him. Taking this situation into programming we get the concept of functions. A function is a block of reusable code that can be used to perform a particular function.

## Basics of Functions
### Defining a Function
Defining a function in python requires the use of the keyword **def** followed by the function name and arguments.<br/>
_**Syntax:**_
```python
def function_name(argument1,argument2):
	# Code for the function   
	return <expression>
```
You can call the function by:
```python
function_name(arg1,arg2)
```
where arg1,arg2 are the values you are passing to the function.
### Sample Function
Let's define a function to print the sum of two numbers.
```python
def sum(a,b):
	c=a+b
	print "Sum is",c
```
Now we can call the function:
```python
sum(5,10)
sum(4,8)
```
The output for the following function call will be
```
Sum is 15
Sum is 12
```
Here on the first function call a takes the value 5 and b takes 10. So the output is `Sum is 15`. And in the second function call a takes 4 and b takes 8 and output is `Sum is 12`.
### Return Statement
The **return** statement can be used exit a function.It can also be used to send a value back from the function.<br/>
Syntax is :-
```python
return <expression>
```
Let's take a look at a function to calculate the sum of first **n** natural numbers :-
```python
def sum(n):
	s = 0
	while(n >= 0):
		s += n
		n -= 1
	return s
```
And we can call this function :-
```python
su = sum(10)
print "sum is",su
```
And the output will be:-
```
sum is 55
```
Here the sum is calculated by the function and stored in the variable **s**. The value in s is returned by the function and stored in a variable **su** and thus we get the output mentioned above.

## More on functions
### Global and local variables
The variables declared within a function is a local variable of that particular function. And the variables outside a function is called global variable. The value of the variable depends on the scope of the variable.<br/>
Consider the following code:-
```python
def func(a,b):
	out = a*b
	print "Output is",out
out = 50
func(6,7)
print "Output is",out
```
The output of the following code will be:-
```
Output is 42
Output is 50
```
Here the global **out** is given the value 50. When the `func()` is called, the local **out** will have a value 42 since preference is given to local variables and we get `Output is 42`. After the function is called, the global **out** comes into scope. So for the second print **out** will have the value 50 and thus we get `Output is 50`.
### Default aruments 
You can set a default value for a particular argument of a function. So when you dont pass a certain argument to the function the function will assume the default value for that argument.<br/>
Consider the following code:-
```python
def greet(name,grt = "Hi"):
	print grt,name
greet("Robert")
greet("Joe","Hello")
```
The output for the code will be:-
```
Hi Robert
Hello Joe
```
On the first call of `greet()`, only one argument is given so grt assumes its default value `Hi` and we get the output `Hi Robert`. On the second call two arguments are passed to the funtion and so the `Hello` will overwrite the defalt value of **grt** and thus we will get the output `Hello Joe`.
### Recursive function
Recursion is a tricky concept. It can be used when a certain problem can be divided into smaller sections of the same problem.Just consider calculating the factorial of a number. `6!` is `6*5*4*3*2*1` which can be written as `6x5!`, similarly `5!` can be written as `5x4!` and so on. So as you can see calculating factorial of 5 is divided into smaller and similar task.<br/>
Now with this idea lets write a recursive function which returns the factorial of a number:-
```python
def factorial(n):
	if(n==0):
		return 1
	return n*factorial(n-1)
```
So the above function will return the factorial of a number. Now we can call the function:-
```python
f = factorial(6)
print "6! is",f
```
The output is predictable, but still :-
```
6! is 720
```
In the factorial() the `n==0` is called ext condition. We need the recursive function call to stop at when this condtion is satisfied. And we returned **1** in that case because `0! = 1`. The statement `n*factorial(n-1)` will execute the recursion process.<br/>
In the above mentioned case, for `6!` we will get `6*factorial(5)`, and for `5!` we will get `5*factorial(4)` and so on till **n** is 0, In that case that particular call will return 1 to its previous call, and that that function call will return whatever value it will get to its previous function call and after all the returning back to the previous funtion call it will return the desired value.

## Imported Functions
Till now we spend time defining functions to do a particular task. Now we can use pre-defined functions to do a particular task. All we have to do is import the library which contains that particular function.<br/>
The syntax to import a function from a module is :-
```python
from <module> import <function>
```
For example, to find the square root of a number you can use `sqrt()` from the math library
```python
from math import sqrt
```
You can call this function like any other function:-
```python
print sqrt(9)
print sqrt(5)
```
And output will be :-
```
3
2.2360679775
```
