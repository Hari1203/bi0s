# Introduction
## What is Python?
**Python** is an object-oriented, high-level language.It emphasizes on code readability.It can be used for both simple programming as well as large scale programming.
## Why Python?
Python is basically a scripting language and you can use python to do pretty much anything from making and editing files to sending emails and filling online forms.And python contains many libraries which contains function which can significantly reduce programming efforts.
## Installation
Python is preinstalled for most linux based systems.In case python is not installed you can install it by giving the command
```console
sudo apt install python2.7
```
For installing python 3 you can use the command
```console
sudo apt install python3
```
### PIP
**PIP** is a command-line utility lets you install, reinstall and uninstall python packages. Normally pip comes preinstalled with python. If it’s not installed you can install it by using the command
```console
sudo apt-get install python-pip
```
Once pip is installed you can use it install necessary python packages by using the command 
```console
pip install <package name>
```
