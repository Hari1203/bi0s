# Advanced Python
If you have reached this section, we assume that you are well acquainted with the basic Python stuffs like the basic data types, functions, loops and conditional flow of control statements. Here we discuss python libraries and the usage of python for some basic scripting. Although we use python extensively for automation of repetitive tasks, the application of Python does not stop at that. It can be used to perform a variety of tasks and the built-in libraries only enhances the possibilities. Python has a lot of pre-defined modules known as libraies which does a particular function without the need of a code and knowing your libraries is quintessential when working with Python. Scripting is made real easy with the help of these libraries as we won't be spending time to write code that much.

## Python Libraries

Python libraries are pre-coded modules written primarily in C ,which offers a wide range of facilities and system functionalities which would be otherwise impossible for Python programmers to work with, and also modules written in python which performs common tasks occuring in everyday programming. As mentioned before Python libraries also define datatypes for specific purposes. For instance, the datetime module helps in defining basic date and time data types. This would otherwise require a couple of objects and lists. The pre-defined module spares us the trouble of defining them.  In short libraries are a bulk collection of pre-coded modules that allows you to perform a number of actions without writing your own code. Most library commands follow a particular syntax which accepts it's parameters in a particular order which should be followed. 

These libraries contain a number of components: apart from performing tasks, they also help define data types such as lists and also defines exceptions. These libraries need not be imported using the ```import ``` command as these modules are pre-defined even though most of them are not used commonly. It is imperative that you must know the libraries and where they can be used effectively. For instance when working with data; numpy, scipy, pandas, etc. are the libraries you must know. They have very convenient data transformation functions that will be a real time saver. Applying the libraries effectively makes the code compact and saves a lot of time which would be otherwise spent writing code for the particular algorithm.

## File Handling: Reading and Writing 

In this section we discuss reading and writing 2 kinds of files: text files and binary files. Text files, as it's name suggests contains plain text, ie strings alone. They contain basic text charecters without any details in font, color or size. Files with the extention .txt are all text files which can be opened in  Notepad or any other text editor. Binary files include pdfs, spreadsheet or .exe files. These files when opened in a text editor would be a disorderly muddle, thus it cannot be read like plain text in text editors. Luckily Python has got modules to read text files as well as binary files. We will discuss how to handle both text file and binary files.

### Handling text files
#### Opening a text file
Text files can be opened with the ```open()``` function which would return a file object. Then we use the ```read()``` function to read the file object and ```write()``` function to write on the file object. While using the ```open()``` function, we specify the path of the file as the parameter. In Ubuntu, it looks something like this
```sh
>>> obj = open('/your_file_directory/text.txt')
```
In Windows the ```/``` is replaced with ```\\``` and also specify the drive name, for instance ```C:``` to specify the path.

In the given instance, we store the file object in a variable and now represents the file itself. By default, ```open()``` command opens the file in read only mode thus we cannot make changes in the file. We could explicitly specify this by giving ```'r'``` argument with the path.
```sh
>>> obj = open('/your_file_directory/text.txt', 'r')
```
Thus this can only be read and cannot be written. To overwrite the file, we specify it explicitly by giving the argument ```'w'``` and to append we give ```a```
```sh
>>> obj = open('/your_file_directory/text.txt', 'w')
```
If the given file name does not exist when opened in write mode or append mode, it creates a new text file of the given name in the given directory. After opening a file you should use the ```close()``` function to close the file.
#### Reading a file
As specified before text files can be read using the ```read()``` function. Now that we already created the file object, the basic syntax to read the file object looks like this. 
```py
>>> str = obj.read()
>>> obj.close()
>>> str
'Hello world!'
```
As you can see, the ```read()``` functions returns the contents of the file as a single string which is stored in the variable ```content```. For getting the strings in each line we use the method ```readline()``` to store each line as a single string. For example consider this file ```poem.txt```:


>We don't need no education
We dont need no thought control
No dark sarcasm in the classroom
Teachers leave them kids alone

Each line is ended by the new line character. To get each line in the form of a string we use ```readline()``` function.

```py
>>> obj = open('poem.txt')
>>> obj.readlines()
>>> ['We don\'t need no education\n', 'We don\'t need no thought control\n', 'No dark sarcasm in the classroom\n', 'Teachers leave them kids alone']
```
Here we get each line which is seperated by the ```\n``` character is treated as a seperate string. This makes it easier to comprehend, compared to a single large string of all contents.
#### Writing to a file
Let's now move on to writing on a file object. After explicitly declaring the file to open in write mode by giving ```'w'``` or ```'a'```, we get a file object which can be written on. This will allow overwriting the existing file. We use the ```write()``` function to write upon a file object. The ```write()``` function either overwrites or  appends the existing string according to the argument given. It is similar with working with the value inside a string. And we should use escape sequences to represent non-representative characters like ```'\n'```.

Let's create a new file using ```open()``` and write its contents using ```write()```.
```py
>>> Hello = open('hello.txt', 'w')
>>> Hello.write('Hello world!')
12
>>> str = Hello.read()
>>> Hello.close()
>>> print(str)
Hello world!
```
And that's it folks! You have now leant text file handling... Well, at least the basics.

### Handling Binary files
Binary storage of data inside files is commonly used over ASCII to pack data much more densely while providing much faster access. Converting ASCII to internal binary representations of data that the computer uses takes a lot of time. So Binary storage is clearly more effective compared to plain text. However working with them is not as complicated. In Linux, using the ```hexdump``` would give us the strings. But it would be difficult when there are several objects.
#### Opening Binary files
To open a binary file you just need to add a ```'b'``` next to the modifiers ie ```'r'```, ```'w'``` etc. Then it is opened in binary mode. For instance
```sh
>>>output_file = open("myfile.bin", "wb")
```
This opens the binary file myfile.bin in writing mode. Also in this case too, ```'wb'``` overwrites the existing file. It is quite similar to opening text files.

#### Reading Binary files
There is a module in Python called pickle. This makes the job of reading and writing Binary files relatively easy. These provide us with the ability to convert objects into bitstreams to store in the file and back to reconstruct the original data. Before we begin we should first import the module pickle.
```py
>>> import pickle
```
To read the contents in a Binary object, use ```pickle.load()```. This function reads and interprets the bitstream and reconstructs it to its original object form. 
#### Writing to Binary files
In the ```pickle ``` module there is a function called ```pickle.dump()```. It writes the pickled form or the bitstream of the given object into the file.
We will show an instance on how the given commands work.
```py
>>> import pickle
>>> output_file = open("myfile.bin", "wb")
>>> str="Hello World!"
>>> pickle.dump(str, output_file)
>>> output_file.close()
>>> input_file = open("myfile.bin", "rb")
>>> str = pickle.load(input_file)
>>> print("%s", str )
Hello World!
input_file.close()
```
In the above snippet, you open/create a binary file and writes the string "Hello World!"  in it. Then you close the current instance and create another instance of the file. We then use ```pickle.load()``` to read the contents in the file. As mentioned before, it reconstructs the bitstream to give the original string back.

And there you  have it. You can now read and write both text files as well as binary files. 
Have fun!!